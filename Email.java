import java.util.Date;

public class Email{

	public String toAddress;
	public String fromAddress;
	public String subject;
	public String message;
	public Date date;


	public Email(String toAddress, String fromAddress, String subject, String message){
		this.toAddress = toAddress;
		this.fromAddress = fromAddress;
		this.subject = subject;
		this.message = message;
	}


	public boolean send(){

		System.out.println("Sending Email...");
		System.out.println("TO: " + this.toAddress);
		System.out.println("FROM: " + this.fromAddress);
		System.out.println("SUBJECT " + this.subject);
		System.out.println("MESSAGE:" + this.message);


		this.date = new Date();
		System.out.println("SENT: " + this.date.toString());

		return true;
	}



}